package ru.t1.rleonov.tm;

import java.util.Scanner;

import static ru.t1.rleonov.tm.constant.TerminalCommands.*;

import static ru.t1.rleonov.tm.constant.TerminalArguments.*;

public final class Application {

    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

    private static void processCommands() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_HELP:
                showHelp();
                break;
            case CMD_EXIT:
                exitApplication();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArguments(final String[] args) {
        final String arg = args[0];
        processArgument(arg);
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case CMD_ARG_ABOUT:
                showAbout();
                break;
            case CMD_ARG_VERSION:
                showVersion();
                break;
            case CMD_ARG_HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Task Manager");
        System.out.println("Developer: Roman Leonov");
        System.out.println("Gitlab: https://gitlab.com/kaba4onok/");
        System.out.println("E-mail: rleonov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show application version.\n", CMD_VERSION, CMD_ARG_VERSION);
        System.out.printf("%s, %s - Show application info.\n", CMD_ABOUT, CMD_ARG_ABOUT);
        System.out.printf("%s, %s - Show application commands.\n", CMD_HELP, CMD_ARG_HELP);
        System.out.printf("%s - Exit application.\n", CMD_EXIT);
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument is not supported.");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command is not supported.");
    }

    private static void exitApplication() {
        System.exit(0);
    }

}
