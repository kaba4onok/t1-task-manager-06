package ru.t1.rleonov.tm.constant;

public final class TerminalCommands {

    public final static String CMD_VERSION = "version";
    public final static String CMD_ABOUT = "about";
    public final static String CMD_HELP = "help";
    public final static String CMD_EXIT = "exit";

}
